import React from 'react'
import './App.css'
import News from './News'

const App = () => {
  return (
    <div className="App">
      <div className="head">
        <h1>HN Feed</h1>
        <h2>
          We <i className="fas fa-heart red"></i> hacker news!
        </h2>
      </div>
      <News />
    </div>
  )
}

export default App
