import React, { useEffect, useState } from 'react'
import axios from 'axios'
import moment from 'moment'

const News = () => {
  const [news, setNews] = useState([])

  useEffect(() => {
    getReadedNews()
    fetchNews()
    // eslint-disable-next-line
  }, [])

  // FNCTIONS
  const getReadedNews = () => {
    if (!localStorage['deletedStories']) {
      localStorage.setItem('deletedStories', JSON.stringify([]))
    }
  }

  const fetchNews = async () => {
    const { data } = await axios.get('http://localhost:8000/api/news')
    let filteredNews = data.filter(
      (item) => !JSON.parse(localStorage['deletedStories']).includes(item.id)
    )
    setNews(filteredNews)
  }

  function storage(id) {
    let deletedStories = JSON.parse(localStorage['deletedStories'])
    deletedStories.push(id)
    localStorage.setItem('deletedStories', JSON.stringify(deletedStories))
  }

  function filterStories() {
    let filteredNews = news.filter(
      (item) => !JSON.parse(localStorage['deletedStories']).includes(item.id)
    )
    console.log('filteredNews:', filteredNews)
    setNews(filteredNews)
  }

  return (
    <div>
      <ul>
        {!news ? (
          <p>Loading please wait...</p>
        ) : (
          <ul>
            {news
              .sort((a, b) => b.created_at_order - a.created_at_order)
              .map((newItem) => (
                <li key={newItem.created_at}>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href={newItem.url}
                    className="row"
                  >
                    <p className="title">
                      <strong>{newItem.story_title}</strong>
                    </p>
                    <p className="author">- {newItem.author} -</p>
                    <p className="time">
                      {moment(newItem.created_at).fromNow()}
                    </p>

                    <p
                      onClick={(e) => {
                        e.preventDefault()
                        storage(newItem.id)
                        filterStories()
                      }}
                      className="trash"
                    >
                      <i className="fas fa-trash-alt"></i>
                    </p>
                  </a>
                </li>
              ))}
          </ul>
        )}
      </ul>
    </div>
  )
}

export default News
